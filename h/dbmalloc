/* Copyright 2003 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Debugging malloc
 */

/*
 * Define this for a debugging malloc.  Having DEBUG set too is
 * a good idea if you want to see the messages...
 */

#define DBMALLOC 0

#if DBMALLOC

#ifdef malloc
#undef malloc
#endif
extern void *dbmalloc_malloc (size_t size, char *filename, int line);
#define malloc(size) dbmalloc_malloc (size, __FILE__, __LINE__)

#ifdef free
#undef free
#endif
extern void dbmalloc_free (void *ptr, char *filename, int line);
#define free(ptr) dbmalloc_free (ptr, __FILE__, __LINE__)

#ifdef realloc
#undef realloc
#endif
extern void *dbmalloc_realloc (void *ptr, size_t size, char *filename, int line);
#define realloc(ptr, size) dbmalloc_realloc (ptr, size, __FILE__, __LINE__)

#ifdef calloc
#undef calloc
#endif
extern void * dbmalloc_calloc (size_t nmemb, size_t size, char *filename, int line);
#define calloc(nmemb, size) dbmalloc_calloc (nmemb, size, __FILE__, __LINE__)

#ifdef checkheap
#undef checkheap
#endif
extern void dbmalloc_checkheap (char *filename, int line, Bool full);
#define checkheap(full) dbmalloc_checkheap (__FILE__, __LINE__, full)

#else

#define checkheap(full)

#endif

