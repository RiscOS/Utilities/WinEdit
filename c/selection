/* Copyright 2003 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*-*-C-*-
 *
 * Code to handle selections
 */

#include "resed.h"

static int selwindow = -1;            /* Window with the selection; -1 if we don't own it */
static Bool temporary = FALSE;        /* It's a temporary selection */

/*
 * gain selection in the given window.  If another
 * of our windows currently has it, deselect all in that window first.
 */

error * selection_claim (int window)
{
    if (selwindow == window)
        return NULL;
    else if (selwindow != -1)
        ER (selection_lose () )
    selwindow = window;
    temporary = FALSE;
    return NULL;
}


error * selection_lose ()
{
    void *closure;
    RegistryType type = registry_lookup_window (selwindow, &closure);

    switch (type)
    {
    case Document:
        ER ( document_lose_selection ((DocumentPtr) closure) );
        break;
    case Template:
        ER ( template_lose_selection ((ResourcePtr) closure) );
        break;
    }

    selwindow = -1;
    temporary = FALSE;
    return NULL;
}

error * selection_giveup (int window)
{
    if (window == selwindow)
    {
        selwindow = -1;
        temporary = FALSE;
    }
    return NULL;
}


/*
 * Pass NULL if you don't care about the temp setting
 */

int selection_current (Bool *istemp)
{
    if (istemp)
        *istemp = temporary;
    return selwindow;
}


void selection_set_temporary (Bool temp)
{
    temporary = temp;
}
